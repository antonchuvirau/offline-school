// Main scripts for the project
'use strict';
//Variables
let selectElButton = document.querySelector('.select__btn');
let innerSelectElButton = document.querySelector('.inner-select__btn');
let headerElement = document.querySelector('.header');
let formInputs = document.querySelectorAll('.form__input input');
let privacyCheckboxInputElement = document.querySelector('.privacy-checkbox__input');
let textareaBtnElement = document.querySelector('.form__textarea-btn');
let testimonialsCarousel;
let weCarousel;
//Events
window.addEventListener('scroll', function(){
    if (pageYOffset>10){
        headerElement.classList.add('header_scrolled');
    }
    else{
        headerElement.classList.remove('header_scrolled');
    }
});
//Input
for(let item of formInputs){
    item.addEventListener('focus', (event) => {
        item.parentElement.classList.add('form__input_focused');
        item.nextElementSibling.classList.add('form__label_active');
    });
    item.addEventListener('blur', (event) => {
        if (item.value){
            return;
        }
        else{
            item.parentElement.classList.remove('form__input_focused');
            item.nextElementSibling.classList.remove('form__label_active');
        }
    });
}
//Checkbox
if (privacyCheckboxInputElement){
    privacyCheckboxInputElement.addEventListener('click', function(){
        if (this.checked){
            this.closest('.checkbox').previousElementSibling.lastElementChild.removeAttribute('disabled');
        }
        else{
            this.closest('.checkbox').previousElementSibling.lastElementChild.setAttribute('disabled', 'disabled');
        }
    });
}
//Select
if (selectElButton){
    selectElButton.addEventListener('click', function(){
        this.classList.toggle('select__btn_active');
        this.nextElementSibling.classList.toggle('dropdown_opened');
    });
}
if (innerSelectElButton){
    innerSelectElButton.addEventListener('click', function(){
        this.classList.toggle('inner-select__btn_active');
        this.nextElementSibling.classList.toggle('dropdown_opened');
    });
}
//Testimonials
testimonialsCarousel = new Swiper('.testimonials__carousel', {
    effect: 'fade',
    fadeEffect: {
        crossFade: true
    },
    navigation: {
        nextEl: '.testimonials__btn-next',
        prevEl: '.testimonials__btn-prev',
    },
    pagination: {
        el: '.testimonials__pagination',
        type: 'bullets',
    }
});
//We
weCarousel = new Swiper('.we__carousel', {
    slidesPerView: 4,
    spaceBetween: 30,
    navigation: {
        nextEl: '.we__btn-next',
        prevEl: '.we__btn-prev',
    }
});
//Modal
$('[data-modal]').on('click', function() {
    $($(this).data('modal')).modal();
    return false;
});
//Content list
$(document).on('click', '.content-list__title', function(){
    $(this).toggleClass('content-list__title_active');
    $(this).next().slideToggle();
});
//Form
if (textareaBtnElement){
    textareaBtnElement.addEventListener('click', function(){
        this.classList.toggle('form__textarea-btn_active');
        this.nextElementSibling.lastElementChild.classList.toggle('form__textarea_opened');
    });
}